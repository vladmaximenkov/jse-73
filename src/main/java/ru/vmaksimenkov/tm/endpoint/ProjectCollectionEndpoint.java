package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.api.resource.IProjectCollectionResource;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.service.ProjectRecordService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(value = "/api/projects", produces = MediaType.APPLICATION_JSON_VALUE)
@WebService(endpointInterface = "ru.vmaksimenkov.tm.api.resource.IProjectCollectionResource")
public class ProjectCollectionEndpoint implements IProjectCollectionResource {

    @NotNull
    private final ProjectRecordService service;

    @Autowired
    public ProjectCollectionEndpoint(@NotNull final ProjectRecordService service) {
        this.service = service;
    }

    @NotNull
    @Override
    @WebMethod
    @GetMapping
    public Collection<ProjectRecord> get() {
        return Objects.requireNonNull(service.findAll());
    }

    @Override
    @WebMethod
    @PostMapping
    public void post(@NotNull @WebParam(name = "projects") @RequestBody final List<ProjectRecord> projects) {
        for (ProjectRecord projectRecord : projects) {
            service.merge(projectRecord);
        }
    }

    @Override
    @WebMethod
    @PutMapping
    public void put(@NotNull @WebParam(name = "projects") @RequestBody final List<ProjectRecord> projects) {
        for (ProjectRecord projectRecord : projects) {
            service.merge(projectRecord);
        }
    }

    @Override
    @WebMethod
    @DeleteMapping
    public void delete() {
        service.removeAll();
    }

}
