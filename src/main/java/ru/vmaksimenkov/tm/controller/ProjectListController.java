package ru.vmaksimenkov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.vmaksimenkov.tm.model.CustomUser;
import ru.vmaksimenkov.tm.service.ProjectService;

@Controller
public class ProjectListController {

    @Autowired
    private ProjectService service;

    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", service.findAll());
    }

}
