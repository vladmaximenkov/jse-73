package ru.vmaksimenkov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.CustomUser;
import ru.vmaksimenkov.tm.service.TaskRecordService;

@Controller
public class TaskController {

    @Autowired
    private TaskRecordService service;

    @GetMapping("/task/create")
    public String create() {
        @NotNull final TaskRecord task = new TaskRecord("New project " + System.currentTimeMillis());
        service.merge(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        service.removeById(id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        @Nullable final TaskRecord task = service.findById(id);
        return new ModelAndView("task-edit", "task", task);
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") TaskRecord task,
            BindingResult result
    ) {
        service.merge(task);
        return "redirect:/tasks";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

}
