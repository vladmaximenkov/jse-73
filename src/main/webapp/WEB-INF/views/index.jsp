<jsp:include page="../include/_header.jsp"/>
<style>
    form {
        display: inline-block;
    }
    button {
        width: 150px;
    }
</style>
<h1>Welcome to the Task Manager.</h1>
<form method="get" action="/projects">
    <button type="submit">Project</button>
</form>
<form method="get" action="/tasks">
    <button type="submit">Task</button>
</form>
<jsp:include page="../include/_footer.jsp"/>