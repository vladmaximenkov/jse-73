<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>Edit task</h1>

<form:form action="/task/edit/${task.id}/" method="POST" modelAttribute="task">
    <table>
        <tr>
            <td>Name:</td>
            <td><form:input type="text" path="name"/></td>
        </tr>
        <tr>
            <td>Description:</td>
            <td><form:input type="text" path="description"/></td>
        </tr>
        <tr>
            <td>Status:</td>
            <td><form:select path="status">
                <!--form:option value="${null}" label="--- // ---"/-->
                <form:options items="${statuses}" itemLabel="displayName"/>
            </form:select></td>
        </tr>
        <tr>
            <td>Date begin:</td>
            <td><form:input type="date" path="dateStart"/></td>
        </tr>
        <tr>
            <td>Date finish:</td>
            <td><form:input type="date" path="dateFinish"/></td>
        </tr>
    </table>
    <form:input type="hidden" path="id"/>
    <button type="submit">Save</button>
</form:form>

<jsp:include page="../include/_footer.jsp"/>
